# About

With the JobWizard you can create a responsive job advertisement free of charge, quickly and easily. All you need to do is fill in the fields and you will end up with a stand-alone HTML page. You can submit the ad to the job board of your choice for publication.

Or you can register in the Job Wizard. Then they can manage your job ad (coming soon.)

## Status

currently under development. But the following is already working

* Login, register and everything related to authentication (Thanks to [Keycloak](https://www.keycloak.org/))
* Entering a job ad (Thanks to [Quasar](https://quasar.dev))
* Download of a job ad with Google for Jobs Markup.
* Integration of Google Places API 
* configure colors and layout


## Roadmap

There is [Google Jobs](https://de.wikipedia.org/wiki/Google_for_Jobs). A great service that makes job ads findable via Google. But there is no entry tool for ads. We want to close this gap.

* save Jobs (give jobs an URI)
* join a newsletter
* more documentation